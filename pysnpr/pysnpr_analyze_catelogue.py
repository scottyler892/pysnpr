#!/usr/bin/env python3

import argparse
import fileinput
from copy import deepcopy
try:
    from pyminer.common_functions import *
    from pyminer.pyminer_process_tad_bed import *
    from pyminer.pyminer_ensembl_rest import map_variants_to_genes
    from pyminer.pyminer_gene_term_comentions import get_comention_summary
    from pyminer.pyminer_gprofiler_converter import convert_to_ensg
    from pyminer.pyminer_annotate_variants_to_genes import do_var_to_ensg_mapping
except:
    from common_functions import *
    from pyminer_process_tad_bed import *
    from pyminer_ensembl_rest import map_variants_to_genes
    from pyminer_gene_term_comentions import get_comention_summary
    from pyminer_gprofiler_converter import convert_to_ensg
    from pyminer_annotate_variants_to_genes import do_var_to_ensg_mapping

################################################################

def get_all_traits(gwas_file):
	all_traits = []
	first = True
	for line in fileinput.input(gwas_file):
		temp_line = strip_split(line)
		if first:
			for i in range(0,len(temp_line)):
				if temp_line[i] == "DISEASE/TRAIT":
					disease_index = i
			first = False
		else:
			if temp_line[disease_index] not in all_traits:
				all_traits.append(temp_line[disease_index])
	fileinput.close()
	trait_dict = {trait:[] for trait in all_traits}
	return(trait_dict)


def populate_dict(trait_dict, gwas_file):
	first = True
	for line in fileinput.input(gwas_file):
		temp_line = strip_split(line)
		if first:
			for i in range(0,len(temp_line)):
				if temp_line[i] == "DISEASE/TRAIT":
					disease_index = i
				if temp_line[i] == "SNPS":
					snp_index = i
			first = False
		else:
			temp_disease = temp_line[disease_index]
			temp_traits = trait_dict[temp_disease]
			temp_traits.append(temp_line[snp_index])
			trait_dict[temp_disease] = temp_traits
	fileinput.close()
	return(trait_dict)


def map_traits_to_vars(gwas_file):
	## first catelogue all of the EFO terms
	trait_dict = get_all_traits(gwas_file)
	trait_dict = populate_dict(trait_dict,gwas_file)
	return(trait_dict)


def remove_non_numeric(s):
	out_str = 'rs'
	for i in range(2,len(s)):
		#print(s[i])
		try:
			blah = int(deepcopy(s[i]))
		except:
			pass
		else:
			out_str += str(s[i])
	return(out_str)


def filter_for_rsIDs(temp_vars):
	first_out_vars = []
	for temp_var in temp_vars:
		if temp_var[:2]=='rs' and temp_var not in first_out_vars:
			first_out_vars.append(temp_var)
	second_out_vars = []
	for temp_var in first_out_vars:
		temp_var = temp_var.split(';')
		for var in temp_var:
			if var not in second_out_vars:
				temp_var = var.strip()
				temp_var = remove_non_numeric(removeNonAscii(temp_var))
				second_out_vars.append(temp_var)
	second_out_vars = sorted(list(set(second_out_vars)))
	return(second_out_vars)

def analyze_all_variants(gwas_file,
	                     #efo_mapping_file,
	                     out):
	out = process_dir(out)
	trait_to_var_dict = map_traits_to_vars(gwas_file)
	for trait, var_list in trait_to_var_dict.items():
		trait_str = deepcopy(trait).replace('/',' ')
		trait_str = trait_str.replace(' ','_')
		temp_out_dir = process_dir(os.path.join(out,trait_str))
		done_file = os.path.join(temp_out_dir,"done")
		if not os.path.isfile(done_file):
			temp_var_file = os.path.join(temp_out_dir,'variants.txt')
			make_file('\n'.join(deepcopy(filter_for_rsIDs(var_list))),temp_var_file)
			temp_search_term_file = os.path.join(temp_out_dir,'disease_terms.txt')
			make_file(deepcopy(str(trait)),temp_search_term_file)
			print("working on", trait)
			# do_var_to_ensg_mapping(variant_file,
			#                            out_dir,
			#                            species,
			#                            lib_dir,
			#                            annotation_file = None,
			#                            id_list_file = None,
			#                            search_terms = None,
			#                            no_search_quote = False)
			do_var_to_ensg_mapping(variant_file = temp_var_file, 
				                   out_dir = temp_out_dir,
				                   species = 'hsapiens',
				                   lib_dir = "/home/scott/bin/pyminer/lib",
				                   annotation_file = None,
				                   id_list_file = None,
				                   search_terms = temp_search_term_file,
				                   no_search_quote = True)
			make_file('',done_file)
	return

############################################################

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	
	parser.add_argument("-gwas", 
	    help="the file containing gwas results file obtained from: ftp://ftp.ebi.ac.uk/pub/databases/gwas/releases/latest/gwas-efo-trait-mappings.tsv")

	# parser.add_argument("-efo_trait_mapping_file", "-efo",
	#     help="the file containing gwas efo trait mapping file obtained from: ftp://ftp.ebi.ac.uk/pub/databases/gwas/releases/latest/gwas-catalog-associations_ontology-annotated.tsv")
	
	parser.add_argument("-out",
	    help="directory for output")
	 
	args = parser.parse_args()
	analyze_all_variants(gwas_file = args.gwas,
	                     out = args.out)
