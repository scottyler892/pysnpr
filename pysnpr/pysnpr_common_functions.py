import os
import site, sysconfig

####################################################################
def find_lib_file(find_file, attempt_dir="", repo_name = "pysnpr"):
    """Finds a lib directory in several possible locations for the given repository.
    
    Args:
        find_file (STR): The file we're looking for within the attempt_dir (usually the lib dir of the repo).
        attempt_dir (STR): A first guess at where the lib file might be.
        repo_name (str, optional): Description
    
    Returns:
        correct_dir (STR): The successfully found directory, or None type if we couldn't find it
    """
    ## double check if it exists!
    putative_human_file = os.path.join(attempt_dir,find_file)
    if not os.path.isfile(putative_human_file):
        print("we couldn't find the string-db interactions in the supplied string_db dir:")
        print(attempt_dir)
        print("will look around a bit!")
        ## gather a bunch of possible locations dependeing on if being run as a script or not
        no_import = False
        try:
            from pyminer import pyminer_objs
        except:
            print("couldn't import! This must be a script")
            no_import = True
        
        dirs_to_try = []
        if not no_import:
            dirs_to_try.append(os.path.dirname(pyminer_objs.__file__))
        if "getsitepackages" in dir(site):
            site_packs = site.getsitepackages()
            for i in range(len(site_packs)):
                site_packs[i]=os.path.join(site_packs[i],repo_name)
            dirs_to_try += site_packs
        dirs_to_try.append(os.path.join(sysconfig.get_paths()["purelib"],repo_name))
        ## now try the locations
        found_it = False
        for test_dir in dirs_to_try:
            putative_relative_human_file = os.path.join(test_dir,find_file)
            print(putative_relative_human_file)
            if os.path.isfile(putative_relative_human_file):
                print("\n\nFound it!\n", "using this as the attempt_dir:",test_dir)
                attempt_dir = test_dir
                found_it = True
        if not found_it:
            print("\n\n\n\nWARNING! couldn't find the right files in the attempt_dir:",attempt_dir)
            return(None)
    return(correct_dir)