# README #

### Tutorials ###

I've made some videos that walk you through the outputs as well as how to install and use PySNPr here:
www.ScienceScott.com/pysnpr

### What is this repository for? ###

* 

### How do I get set up? ###

PySNPr is pip installable:
`python3 -m pip install pysnpr`
Note that there was another package called pyminer for mining bit-coin - this is definitely not that, so be sure to install bio-pyminer instead!

You can also install using the setup.py script in the distribution like so:
`python3 setup.py install`


### How do I run PySNPr? ###

For example:
`pysnpr.py -i snp_list.txt -o /out/directory/ -search_terms search_terms.txt`

This will generate 3 files:
* *expression.hdf5* 
* *ID_list.txt* the list of genes (no header line)
* *column_IDs.txt* the sample names for the columns.

Or if you want to do it interactively within python:
`tab_to_h5.py expression.txt`

### License ###
For non-commercial use, PySNPr is available via the AGPLv3 license. Commercial entities should inquire with scottyler89@gmail.com

### Who do I talk to? ###
* Repo owner/admin: scottyler89+bitbucket@gmail.com